# Example use of button in Houdini

Add a button with a callback script:
```python
hou.pwd().hdaModule().blah()
```

Inside the script module, add a python module:

```python
def blah():
    hou.parm(hou.pwd().path()+"/parm2").set(5)
```